//import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Subnet {
	/**
	 * das Array mit den Subnetzaddressen als Strings
	 */
	public LinkedHashMap<String, LinkedHashMap> subnetArr 
		= new LinkedHashMap<String, LinkedHashMap>();
	
	private Integer hostPart;
	public String networkIp;
	
	private static Subnet instance;
	public static Subnet getInstance () {
		if (Subnet.instance == null) {
			Subnet.instance = new Subnet();
	    }
		return Subnet.instance;
    }
	
	/**
	 * @param ip
	 * @return
	 */
	public String decimalToBinaryIp(String ip){
		String[] decValues = ip.split("\\.");
		
		String binaryValue = "";
		String binary = "";
		
		for(int i = 0; i < decValues.length; i++){
			int val = Integer.parseInt(decValues[i]);
			binary = Integer.toBinaryString(val);
			if(binary.length() != 8){
				int fillUp = 8 - binary.length();
				for(int p = 0; p < fillUp; p++){
					binaryValue += "0";
				}
			}
			binaryValue += binary;
		}
		return binaryValue;
	}
	
	public String binaryToDecimalIp(String ip){
		String decimalValue  = "";
		for(int i = 0; i < 32; i = i+8){
			String val = ip.substring(i, i+8);
			decimalValue += Integer.parseInt(val, 2);
				if(i < 24){
					decimalValue += ".";
				}
		}
		
		return decimalValue;
	}
	
	public Boolean calcSubnet(String ip, int subnetcounter){
		String[] networkArr = ip.split("/");
		String binaryIP = Subnet.getInstance().decimalToBinaryIp(networkArr[0]);
		
		int tempVar = new Double(Math.ceil(Math.log(subnetcounter) / Math.log(2))).intValue();
		
		int maxSubnets = (int)Math.pow(2, tempVar);
		
		int newSuffix = Integer.parseInt(networkArr[1]) + tempVar;	
		
		if( newSuffix > 30 ) {
			return false;
		}
		//Hosts per Subnet
		double hosts = Math.pow(2, 32-newSuffix);
		
		//Netzwerk IP berechnen und darauf dann bei jedem Host + HostperSubnet
		
		String netmask = "00000000000000000000000000000000";
		for(int i = 0; i < Integer.parseInt(networkArr[1]); i++){
			netmask = "1" + netmask;
		}
		netmask = netmask.substring(0, 32);
	
//		System.out.println("IPBin�r: "+binaryIP);
//		System.out.println("IP: "+this.binaryToDecimalIp(binaryIP));
		
//		System.out.println("Netzwerkmaske: "+netmask);
//		System.out.println("Netzwerkmaske: "+this.binaryToDecimalIp(netmask));
		
		String wildcard = "00000000000000000000000000000000";
		for(int i = 0; i < 32-(Integer.parseInt(networkArr[1])); i++){
			wildcard += "1";
		}
		wildcard = wildcard.substring(wildcard.length()-32, wildcard.length());
//		System.out.println("Wildcard: "+wildcard);
//		System.out.println("Wildcard: "+this.binaryToDecimalIp(wildcard));
		
		String netzwerkadd = "";
		for(int i = 0; i < 32; i++){
			if(binaryIP.substring(i, i+1).equals("1")){
				if(netmask.substring(i, i+1).equals("1")){
					netzwerkadd += "1";
				}else{
					netzwerkadd += "0";
				}
			}else{
				netzwerkadd += "0";
			}
		}
//		System.out.println("Netzwerkadresse: "+netzwerkadd);
//		System.out.println("Netzwerkadresse: "+this.binaryToDecimalIp(netzwerkadd));
		
		ArrayList subnetArr = new ArrayList();
		
		
		
		for(int i = 0; i < maxSubnets; i++){
			
			String moep = "11111111111111111111111111111111";
			String subnetarr = "";
			int tempoVar = (int)hosts*i;
			String binary = Integer.toBinaryString(tempoVar);
			String binaryaddre = "";
			binaryaddre = netzwerkadd;
		
			binaryaddre = binaryaddre.substring(0, 32-binary.length());
			binaryaddre += binary;
			
			String thatIp = this.binaryToDecimalIp(binaryaddre) + "/" + newSuffix;
			
			LinkedHashMap content = new LinkedHashMap();
			content.put("ip", thatIp);
			content.put("description", "");
			if(i > subnetcounter-1){
				// FLAG FALSE
				content.put("isActive", false);
			} else {
				content.put("isActive", true);
			}
			this.subnetArr.put( thatIp,  content);
			
		}
		
		
		
		// NETWORKADRESS
		
		
		
//		Sting Subnet = ""
		
		return true;
	}
	
	private Subnet( ) { }

	public Boolean selectSubnet( String ip ) {
		LinkedHashMap content = this.subnetArr.get( ip );
		if( content.get("isActive").toString().equals("true") ) {
			Host.getInstance().createHosts( ip );
			return true;
		} else {
			return false;
		}
	}
	
	
	public boolean createSubnets( String inputVal ){
		try {
			int inputAsInt = Integer.parseInt(inputVal);
			if( inputAsInt > 30 ) {
				return false;
			}
			return this.calcSubnet(this.networkIp, inputAsInt);
		} catch (Exception e) {
			return false;
		}
	}

}
