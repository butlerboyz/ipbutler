import java.awt.*;
import java.awt.event.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.*;
import javax.swing.text.JTextComponent;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.RGBColor;

import sun.awt.image.PixelConverter.Rgba;

//import com.google.gson.JsonNull;



public class Gui {

	private String		fensterName		= "IP-Butler";
	private JFrame 		fenster;
	public  JTabbedPane tabLeiste 		= new JTabbedPane();
	public  JPanel 		networkPanel 	= new JPanel();
	public  JPanel 		subnetPanel 	= new JPanel();
	public  JPanel 		hostPanel 		= new JPanel();
	public  JTextField	newNetworkInput = new JTextField();
	public	JList		networkList;
	private DefaultListModel 
						listModel  		= new DefaultListModel();
	private JButton		addNetworkBtn   = new JButton();
	private JButton		selectNetworkBtn= new JButton();
	private String		currentSelectedNetwork;
	
	private static Gui instance;
	public static Gui getInstance () {
		if (Gui.instance == null) {
			Gui.instance = new Gui();
	    }
		return Gui.instance;
    }
	
	private Gui () {

		fenster = new JFrame( fensterName );
		fenster.setSize(500, 500);
		fenster.setLocationRelativeTo(null);
		fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		fenster.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	Manager.getInstance().save();
		    }
		});
		
				
		fenster.add(tabLeiste);
		fenster.setVisible(true);
	}
	
    private NetworkViewClass networkGui;
    private SubnetViewClass subnetGui;
	private HostViewClass hostGui;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Gui gui = Gui.getInstance(); 
		
		// initialize network
		Network.getInstance();
		
		gui.networkGui = gui.new NetworkViewClass();
		gui.subnetGui  = gui.new SubnetViewClass();
		gui.hostGui    = gui.new HostViewClass();
		
		gui.tabLeiste.setEnabled(false);
		gui.networkGui.updateView();
		/**
		 * todo: ascii datei einlesen bzw. erstellen
		 */
		//	gui.ipButler.selectNetwork( "192.168.0.0" );

	}

	
	
	public class NetworkViewClass {
		public  JPanel 		networkPanel 	= new JPanel();
		public  JTextField	newNetworkInput = new JTextField();
		public	JList		networkList;
		private DefaultListModel 
							listModel  		= new DefaultListModel();
		private JButton		addNetworkBtn   = new JButton();
		private JButton		selectNetworkBtn= new JButton();
		private JButton		deleteNetworkBtn = new JButton();
		private String		currentSelectedNetwork;
		
		public NetworkViewClass() {
			
			networkPanel.setBackground(new Color(42, 100, 150));
			// elements in networkPanel ---- 
				// the list model contains all option elements
		        networkList = new JList( listModel );
//		        networkList.setPreferredSize(new Dimension(430, 100));
		        ListSelectionModel listSelectionModel = networkList.getSelectionModel();
		        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						currentSelectedNetwork = (String)networkList.getSelectedValue();
					}
				});
		        JScrollPane scrollPane = new JScrollPane();
		        scrollPane.setViewportView(networkList);
		        scrollPane.setPreferredSize(new Dimension(450, 100));
		        networkPanel.add( scrollPane );

				selectNetworkBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Network network = Network.getInstance();
						Boolean validIp = network.selectNetwork(currentSelectedNetwork);
						if( validIp ) {
							if( network.checkIfNetworkIsEmpty( currentSelectedNetwork ) ){
								Gui.getInstance().subnetGui.showSubnetDialog();
							}
							Gui.getInstance().subnetGui.updateView();
							Gui.getInstance().tabLeiste.setSelectedIndex(1);
							
						}
					}
				});
				selectNetworkBtn.setText( "Netzwerk ausw�hlen" );
				networkPanel.add( selectNetworkBtn );
				
				deleteNetworkBtn.setText("Netzwerk l�schen");
				deleteNetworkBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Network network = Network.getInstance();
						network.deleteNetwork(currentSelectedNetwork);
						updateView();
					}
				});
				networkPanel.add( deleteNetworkBtn );
				
				JLabel b = new JLabel();
		        b.setText("Neues Netzwerk: ");
		        b.setPreferredSize(new Dimension(450, 25));
				networkPanel.add(b);
		        
				newNetworkInput.setPreferredSize(new Dimension(150, 25));
				networkPanel.add( newNetworkInput );
			
		        addNetworkBtn.addActionListener(new ActionListener() { 
		            public void actionPerformed(ActionEvent evt) { 
		            	Network.getInstance().createNetwork( newNetworkInput.getText() );
		            	updateView();
		            }
		          });
		        addNetworkBtn.setText("F�ge Netzwerk hinzu");
		        networkPanel.add( addNetworkBtn );
			
		   Gui.getInstance().tabLeiste.addTab("Netzwerke", networkPanel);
		}

		public String getNewNetworkIp() {
			return newNetworkInput.getText();
		}
		
		public void truncateNetworkList() {
			listModel.removeAllElements();	
		}
		
		public void addElementToNetworkList( String option ) {
			listModel.addElement( option );
		}

		public void updateView() {
			this.truncateNetworkList();
			for(Map.Entry<String, Object> entry : Network.getInstance().networkArr.entrySet()) {
			    String key = entry.getKey();
			   // LinkedHashMap value = entry.getValue();
			        
			    this.addElementToNetworkList( key );
			}
			
//		    Vector data = listModel.getDataVector();
			
//		    Collections.sort(data);
		}
		

	};

	public class SubnetViewClass {
		public  JPanel 				subnetPanelWrapper  	= new JPanel();
		public  JPanel 				subnetPanel 			= new JPanel();
		public  JPanel 				subnetDialog 			= new JPanel();
		private DefaultListModel 	listModel 				= new DefaultListModel();
		public  JTextField			newSubnetInput 			= new JTextField();
		public  JTextField			subnetNumber 			= new JTextField();
		private JButton				addSubnetBtn    		= new JButton();
		private JButton				selectSubnetBtn 		= new JButton();
		private String				currentSelectedSubnet;
		private String 				newSubnet;
		private Integer 			allowedHostPart;
		public	JList				subnetList; 
		public  JTextField			subnetDescription		= new JTextField();
		public  JTextField			isActiveField  			= new JTextField();
		public  JButton				saveBtn					= new JButton();
		
		
		public SubnetViewClass() {
			subnetPanelWrapper.setSize(new Dimension(500, 500));
			subnetPanel.setPreferredSize(new Dimension(500, 500));
			subnetPanel.setBackground(new Color(42, 100, 150));
			// elements in networkPanel ---- 
				// the list model contains all option elements
				subnetList = new JList( listModel );
//		        subnetList.setPreferredSize(new Dimension(450, 100));
		        ListSelectionModel listSelectionModel = subnetList.getSelectionModel();
		        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						if(subnetList.getSelectedValue() != null) {
													
							currentSelectedSubnet = subnetList.getSelectedValue().toString();
							if( currentSelectedSubnet != null ) {
								String[] strArr = currentSelectedSubnet.split("\\| ");		
								currentSelectedSubnet = strArr[1];
								
								LinkedHashMap content = Subnet.getInstance().subnetArr.get(currentSelectedSubnet);
								subnetDescription.setText( content.get("description").toString() );
								isActiveField.setText( content.get("isActive").toString() );
							}
						} else {
							subnetDescription.setText( "" );
							isActiveField.setText( "" );
						}
					}
				});
		        JScrollPane scrollPane = new JScrollPane();
		        scrollPane.setViewportView(subnetList);
		        scrollPane.setPreferredSize(new Dimension(450, 100));
		        subnetPanel.add(scrollPane);
	
		        selectSubnetBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Boolean selectIsValid = Subnet.getInstance().selectSubnet( currentSelectedSubnet );
						if( selectIsValid ) {
							Gui.getInstance().tabLeiste.setSelectedIndex(2);
							Gui.getInstance().hostGui.updateView();
						}
					}
				});
				selectSubnetBtn.setText( "Subnetz ausw�hlen" );
				subnetPanel.add( selectSubnetBtn );
				
		        subnetDescription.setPreferredSize(new Dimension(200, 75));
		        JLabel b = new JLabel();
		        b.setText("Die Beschreibung zu diesem Subnetz: ");
		        b.setPreferredSize(new Dimension(450, 25));
		        subnetPanel.add( b );
		        subnetPanel.add( subnetDescription );		   
		        
		        isActiveField.setPreferredSize(new Dimension(100, 25));
		        JLabel c = new JLabel();
		        c.setText("Ist dieses Subnetz aktiv ?");
		        c.setPreferredSize(new Dimension(450, 25));
		        subnetPanel.add( c );
		        subnetPanel.add( isActiveField );	
		        
		        saveBtn.setText( "Speichern!" );
		        saveBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						LinkedHashMap content = Subnet.getInstance().subnetArr.get(currentSelectedSubnet);
						content.put("description", subnetDescription.getText());
						Boolean b;
						if( isActiveField.getText().equals( "true" ) ) {
							b = true;
						} else {
							b = false;
						}
						content.put("isActive", b);
						Subnet.getInstance().subnetArr.put(currentSelectedSubnet, content);
						updateView();
					}
				});
		        subnetPanel.add( saveBtn );	
		        
		        
	        subnetDialog = new JPanel();
	        subnetDialog.setPreferredSize(new Dimension(500, 500));
		        subnetDialog.setBackground(new Color(42, 100, 150));
				subnetNumber.setPreferredSize(new Dimension(150, 25));
				subnetDialog.add(subnetNumber);
				
				JButton addSubnets = new JButton();
				addSubnets.setText("Subnetze anlegen !");
				addSubnets.addActionListener(new ActionListener() { 
		            public void actionPerformed(ActionEvent evt) {
		            	String subnetsInt = subnetNumber.getText();
		            	Boolean subnetsValid = Subnet
							            		.getInstance()
							            		.createSubnets(subnetsInt);
		            	if( subnetsValid ){
		            		updateView();
		            		showNormalSubnetDialog();
		            	}
		            }
		          });
				
				subnetDialog.add(addSubnets);
				subnetDialog.setVisible(false);
		        subnetPanelWrapper.add( subnetDialog );
		        
			subnetPanelWrapper.add( subnetPanel );
			Gui.getInstance().tabLeiste.addTab("Subnetze", subnetPanelWrapper);
		}
		
		public void showSubnetDialog( ){
			subnetPanel.setVisible(false);
			subnetDialog.setVisible(true);
		}
		public void showNormalSubnetDialog( ){
			subnetPanel.setVisible(true);
			subnetDialog.setVisible(false);
		}

		public String getNewSubnetIp() {
			return newSubnetInput.getText();
		}
		
		public void truncateSubnetList() {
			listModel.removeAllElements();	
		}
		
		public void addElementToSubnetList( LinkedHashMap content ) {
			if( content.get("isActive").toString().equals("true") ) {
				listModel.addElement( " + | " + content.get("ip").toString() );
			} else {
				listModel.addElement( " -  | " + content.get("ip").toString() );
			}
			
		}

		public void updateView() {
			this.truncateSubnetList();
			for(Map.Entry<String, LinkedHashMap> entry : Subnet.getInstance().subnetArr.entrySet()) {
//			    String key = entry.getKey();
			    LinkedHashMap value = entry.getValue();
			    this.addElementToSubnetList( value );
			}
		}
		

	};
	
	public class HostViewClass {
		public  JPanel 				HostPanel 			= new JPanel();
		private DefaultListModel 	listModel 			= new DefaultListModel();
		public  JTextField			newHostInput 		= new JTextField();
		private JButton				addHostBtn    		= new JButton();
		private JButton				selectHostBtn 		= new JButton();
		private String				currentSelectedHost;
		public	JList				HostList; 
		public  JTextField			isActiveField 		= new JTextField();
		public  JButton				saveBtn				= new JButton();
		
		public HostViewClass() {
			HostPanel.setSize(200, 100);
			HostPanel.setBackground(new Color(42, 100, 150));
			// elements in networkPanel ---- 
				// the list model contains all option elements
				HostList = new JList( listModel );
//		        HostList.setPreferredSize(new Dimension(430, 400));
		        ListSelectionModel listSelectionModel = HostList.getSelectionModel();
		        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						currentSelectedHost = (String)HostList.getSelectedValue();
						if(HostList.getSelectedValue() != null) {
							
							currentSelectedHost = HostList.getSelectedValue().toString();
							if( currentSelectedHost != null ) {
								String[] strArr = currentSelectedHost.split("\\| ");		
								currentSelectedHost = strArr[1];
								
								LinkedHashMap content = Host.getInstance().hostArr.get(currentSelectedHost);
								isActiveField.setText( content.get("isActive").toString() );
							}
						} else {
							isActiveField.setText( "" );
						}
					}
				});
		        JScrollPane scrollPane = new JScrollPane();
		        scrollPane.setViewportView(HostList);
		        scrollPane.setPreferredSize(new Dimension(450, 300));
		        HostPanel.add( scrollPane );
		        
		        
		        isActiveField.setPreferredSize(new Dimension(100, 25));
		        JLabel c = new JLabel();
		        c.setText("Ist dieser Host aktiv ?");
		        c.setPreferredSize(new Dimension(450, 25));
		        HostPanel.add( c );
		        HostPanel.add( isActiveField );	
		        
		        saveBtn.setText( "Speichern!" );
		        saveBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						LinkedHashMap content = Host.getInstance().hostArr.get(currentSelectedHost);
						Boolean b;
						if( isActiveField.getText().equals( "true" ) ) {
							b = true;
						} else {
							b = false;
						}
						content.put("isActive", b);
						Host.getInstance().hostArr.put(currentSelectedHost, content);
						updateView();
					}
				});
		        HostPanel.add( saveBtn );
		        
		        selectHostBtn.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
//						Host.getInstance().selectHost( currentSelectedHost );
					}
				});
				selectHostBtn.setText( "Host ausw�hlen" );
//				HostPanel.add( selectHostBtn );
				
				newHostInput.setPreferredSize(new Dimension(150, 25));
//				HostPanel.add( newHostInput );
			
				addHostBtn.addActionListener(new ActionListener() { 
		            public void actionPerformed(ActionEvent evt) { 
//		            	Host
//		            		.getInstance()
//		            		.createHosts( newHostInput.getText() );
		            	updateView();
		            }
		          });
				addHostBtn.setText("F�ge Host hinzu");
//		        HostPanel.add( addHostBtn );
			
			Gui.getInstance().tabLeiste.addTab("Hosts", HostPanel);
		}
		

		public String getNewHostIp() {
			return newHostInput.getText();
		}
		
		public void truncateHostList() {
			listModel.removeAllElements();	
		}
		
		public void addElementToHostList( LinkedHashMap content, String ip ) {
			if( content.get("isActive").toString().equals("true") ) {
				listModel.addElement( " + | " + ip.toString() );
			} else {
				listModel.addElement( " -  | " + ip.toString() );
			}
		}

		public void updateView() {
			this.truncateHostList();
			for(Map.Entry<String, LinkedHashMap> entry : Host.getInstance().hostArr.entrySet()) {
			    String key = entry.getKey();
//			    LinkedHashMap value = entry.getValue();
			    LinkedHashMap value = entry.getValue();
			    this.addElementToHostList( value, key );
			}
		}
		

	};
	
}

