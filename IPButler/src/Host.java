import java.util.*;


public class Host {
	
	/**
	 * das Array mit den Netzwerkenaddressen als Strings
	 */
	public Map<String, LinkedHashMap> hostArr 
		= new LinkedHashMap<String, LinkedHashMap>();
	
	private static Host instance;
	public static Host getInstance () {
		if (Host.instance == null) {
			Host.instance = new Host();
	    }
		return Host.instance;
    }
	private Host( ) { }
	public Integer hostNumb;
	public String newHostIp;
	public double hostNumber;

	public void createHosts( String subnetIp  ) {
		String [] subnetSplit = subnetIp.split("/");
		String [] subnetSplitDet = subnetSplit[0].split("\\.");
		hostNumber = Math.pow(2, 32-Integer.valueOf(subnetSplit[1]))-2;
		Integer Host1 = new Integer(Integer.valueOf(subnetSplitDet[3])+1);
		Integer Host2 = new Integer(Integer.valueOf(subnetSplitDet[2]));
		Integer Host3 = new Integer(Integer.valueOf(subnetSplitDet[1]));
		Integer Host4 = new Integer(Integer.valueOf(subnetSplitDet[0]));
		for(int i = 0;i<hostNumber+1;i++){
			if (Host1 > 255){
				Host1 = 0;
				Host2 = Host2 + 1;
			}
			if (Host2 > 255){
				Host2 = 0;
				Host3 = Host3 + 1;
			}
			if (Host3 > 255){
				Host3 = 0;
				Host4 = Host4 + 1;
			}
			newHostIp= Host4.toString() + "." + Host3.toString() + "." + Host2.toString() + "." + Host1.toString() + "/" + subnetSplit[1];
			LinkedHashMap b = new LinkedHashMap();
			b.put("isActive", false);
			this
				.hostArr
				.put( newHostIp, b );
			Host1 = Host1 +1;
		}
	}
	
	public void setActive( String hostIp ){
		
	}
	
	public void setInactive( String hostIp ) {
		
	}
}

