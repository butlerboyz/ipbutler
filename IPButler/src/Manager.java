import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Manager {
	private static final String PATHNAME = "H:/git/IPButler/network.json";

	private static Manager instance;
	public static Manager getInstance () {
		if (Manager.instance == null) {
			Manager.instance = new Manager();
	    }
		return Manager.instance;
    }
    
    public JsonObject getJsonRecursive( LinkedHashMap<String, Object> map ) {
    	JsonObject wrapper = new JsonObject();
    	for( Map.Entry<String, Object> entry : map.entrySet() ) {
		    String key 			= entry.getKey();
		    Object value 		= entry.getValue();
		    if( value.toString().contains("{") ) {
		    	wrapper.add(key, getJsonRecursive((LinkedHashMap<String, Object>)value));
		    } else {
		    	wrapper.addProperty(key, value.toString());
		    }
		}
    	return wrapper;
    }
    
    // Speichern in datei
    public void toFile(LinkedHashMap<String,Object> map) {
    	JsonObject json = getJsonRecursive(map);
    	
        FileWriter writer;
        try {
            writer = new FileWriter( new File(Manager.PATHNAME) );
            writer.write(json.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public LinkedHashMap<String,Object> getFromFile() {
    	Gson gson = new Gson();
    	LinkedHashMap<String,Object> map = new LinkedHashMap<String,Object>();
    	
    	try {
    		// Datei "netzwerk.json" �ber einen Stream einlesen
    		BufferedReader reader = new BufferedReader(new FileReader(Manager.PATHNAME));
	        
	        
	        // Datei als JSON-Objekt einlesen
	        JsonObject json = gson.fromJson(reader, JsonObject.class);
	        class GetMap {
	        	
	        	public LinkedHashMap<String,Object>  getMapFromStr( String jsonStr ){	
	        		return this.getObjRecursive(jsonStr);
	        	}
	        	
	        	LinkedHashMap<String, Object> getLinkedHashMapFromJson( JSONObject json ){
	        		LinkedHashMap<String, Object> curMapp = new LinkedHashMap<String, Object>();
	        		try {
	    				Iterator<?> keys = json.keys();
		    	        while( keys.hasNext() ){
		    	        	String key = (String)keys.next();
		    	        	
		    	            if( json.get(key) instanceof JSONObject ){
		    	            	curMapp.put(key, getLinkedHashMapFromJson( (JSONObject) json.get(key) ));
		    	            	
		    	            } else {
		    	            	
		    	            	if( json.get(key) != null ){
			    	            	curMapp.put(key, json.get(key));	
		    	            	}
		    	            }
		    	        }
		    	        return curMapp;
					} catch (JSONException e) {
//						System.out.println( "Catched: JSONException in getLinkedHashMapFromJson()" );
						// TODO: handle exception
					}
					return curMapp;
	        	}
	        	
	        	LinkedHashMap<String,Object> getObjRecursive( String string ){
	    			LinkedHashMap<String, Object> curMapp = new LinkedHashMap<String, Object>();
	    			
	    			
	    			try {
	    				JSONObject json = new JSONObject(string.trim());
	    				curMapp = this.getLinkedHashMapFromJson(json);
					} catch (JSONException e) {
//						System.out.println( "Catched: JSONException" );
						// TODO: handle exception
					}
	    			return curMapp;
	            }
	        	
	        	
	    	}
	        GetMap Getter = new GetMap();
	        
	        map = Getter.getMapFromStr(json.toString());
	    	
	        
    	} catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e ){
        	e.printStackTrace();
        }
    	return map;    	
    }
    
    public void save(){
    	this.toFile(Network.getInstance().networkArr);
    }
}
