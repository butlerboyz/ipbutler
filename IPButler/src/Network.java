import java.util.Map;
import java.util.LinkedHashMap;

public class Network {
		
	
	
	/*
	 * networvar Network = new Network();
	 * 
	 {
	 	networks: [
	 		{
	 			ip: 123.5325.1.2134 ....
	 			selectNetwork : function....
	 		}
	 		{
	 			ip: 123.5325.1.2134 ....
	 			selectNetwork : function....
	 		}
	 	]
	 	networks[0].selectNetwork()
	 	netwokrs.getInstance().bef�lleObject().selectNetwokr().l�scheObjectr()
	 }
	  
	 * */
	

	
	/**
	 * das Array mit den Netzwerkenaddressen als Strings
	 */
	public LinkedHashMap<String, Object> networkArr 
		= new LinkedHashMap<String, Object>();

	
	private static Network instance;
	public static Network getInstance () {
		if (Network.instance == null) {
			Network.instance = new Network();
	    }
		return Network.instance;
    }
	private Network () {
		
		// testing
//		networkArr.put( "192.168.10.1/24", new LinkedHashMap() );
//		networkArr.put( "192.168.12845.0/24", new LinkedHashMap() );
//		networkArr.put( "192.168.123128.0/24", new LinkedHashMap() );
//		networkArr.put( "192.168123.128.0/24", new LinkedHashMap() );
//		networkArr.put( "192.162138.128.0/24", new LinkedHashMap() );
//		networkArr.put( "192.163128.128.0/24", new LinkedHashMap() );
//		networkArr.put( "192.135468.128.0/24", new LinkedHashMap() );
//		networkArr.put( "192.1368.128.0/24", new LinkedHashMap() );
		
//		networkArr.put( "192.168.168.0/24", new LinkedHashMap() );
//		networkArr.put( "192.168.255.0/24", new LinkedHashMap() );	
//		System.out.println(networkArr);
		this.networkArr = Manager.getInstance().getFromFile();
//		System.out.println(networkArr);
//		Manager m = new Manager();
//		m.toFile(networkArr);
//		Map<String, LinkedHashMap> u=m.getFromFile();
//		System.out.println(u);
	}
	
	
	/** TODO:
	 * die Methode, um ein Netzwerk zu l�schen
	 * @param networkIp
	 */
	public void deleteNetwork( String networkIp ) {
		this.networkArr.remove(networkIp);
	}
	
	public boolean checkIfNetworkIsEmpty( String networkIp ){
		// if that network is new
		return this.networkArr.get( networkIp ).toString() == "{}";
	}
	
	private boolean checkIpIsValid( String ip ) {		
		try {
			String[] fix = ip.split("/");
			ip = fix[0];
			String suffix = fix[1];

			int Num = Integer.parseInt( suffix );

			if ( (Num < 15) || (Num > 30) ) {
				return false;
			}
			
			if (ip == null || ip.isEmpty()) {
				return false;
			}		
			String[] parts = ip.split( "\\." );
			if ( parts.length != 4 ) {
				return false;
			}		
			for ( String s : parts ) {
				int i = Integer.parseInt( s );
				if ( (i < 0) || (i > 255) ) {
					return false;
				}
			}
			if(ip.endsWith(".")) {
				return false;
			}
		
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}	catch (ArrayIndexOutOfBoundsException nfe) {
			return false;	
		}
		
	}
	
	public boolean selectNetwork( String networkIp ) {
		if( networkIp == null ) {
			return false;
		}
		
		Boolean ipIsValid = this.checkIpIsValid(networkIp);
		
		if( ipIsValid ) {
			Subnet subnetInstance 		= Subnet.getInstance();
			subnetInstance.subnetArr 	= (LinkedHashMap)networkArr.get( networkIp );
			subnetInstance.networkIp 	= networkIp;
		}
		return ipIsValid;
	}
	
	public void createNetwork( String networkIp ){
		Boolean ipIsValid = this.checkIpIsValid(networkIp);
		if( ipIsValid ){
			this
				.networkArr
				.put( networkIp, new LinkedHashMap() );
			Manager.getInstance().toFile(networkArr);
		}
	}

}
